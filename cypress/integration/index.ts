import quandlSuccessStub from "../fixtures/quandlSuccessStub";

describe("Smoke test", function () {
  it("should show input fields", function () {
    cy.intercept("https://www.quandl.com/api/v3/datasets/EOD/MSFT.json*", {
      statusCode: 200,
      headers: { "content-type": "application/json; charset=utf-8" },
      body: JSON.stringify(quandlSuccessStub),
    });

    cy.visit("");
    cy.get("#symbol").should("be.visible");
    cy.get("#startDate").should("be.visible");
    cy.get("#endDate").should("be.visible");
    cy.contains(
      "Microsoft Corporation (MSFT) Stock Prices, Dividends and Splits"
    );
  });
});

describe("Input validation", function () {
  it("should show symbol error", function () {
    cy.intercept("https://www.quandl.com/api/v3/datasets/EOD/MSFT.json*", {
      statusCode: 200,
      headers: { "content-type": "application/json; charset=utf-8" },
      body: JSON.stringify(quandlSuccessStub),
    });

    const selector = "#symbol";
    const errorMessage = "Enter a stock symbol";

    cy.visit("");
    cy.contains(
      "Microsoft Corporation (MSFT) Stock Prices, Dividends and Splits"
    );
    cy.contains(errorMessage).should("not.exist");
    cy.get(selector).clear();
    cy.contains(errorMessage).should("be.visible");
    cy.get(selector).type("MSFT");
    cy.contains(errorMessage).should("not.exist");
  });

  it("should show start date error", function () {
    cy.intercept("https://www.quandl.com/api/v3/datasets/EOD/MSFT.json*", {
      statusCode: 200,
      headers: { "content-type": "application/json; charset=utf-8" },
      body: JSON.stringify(quandlSuccessStub),
    });

    const selector = "#startDate";
    const errorMessage = "Enter valid date";

    cy.visit("");
    cy.contains(
      "Microsoft Corporation (MSFT) Stock Prices, Dividends and Splits"
    );
    cy.contains(errorMessage).should("not.exist");
    cy.get(selector).clear();
    cy.contains(errorMessage).should("be.visible");
    cy.get(selector).type("2014-02-01");
    cy.contains(errorMessage).should("not.exist");
  });

  it("should show end date error", function () {
    cy.intercept("https://www.quandl.com/api/v3/datasets/EOD/MSFT.json*", {
      statusCode: 200,
      headers: { "content-type": "application/json; charset=utf-8" },
      body: JSON.stringify(quandlSuccessStub),
    });

    const selector = "#endDate";
    const errorMessage = "Enter valid date";

    cy.visit("");
    cy.contains(
      "Microsoft Corporation (MSFT) Stock Prices, Dividends and Splits"
    );
    cy.contains(errorMessage).should("not.exist");
    cy.get(selector).clear();
    cy.contains(errorMessage).should("be.visible");
    cy.get(selector).type("2014-02-01");
    cy.contains(errorMessage).should("not.exist");
  });

  it("should validate start with end date", function () {
    cy.intercept("https://www.quandl.com/api/v3/datasets/EOD/MSFT.json*", {
      statusCode: 200,
      headers: { "content-type": "application/json; charset=utf-8" },
      body: JSON.stringify(quandlSuccessStub),
    });

    const selector = "#startDate";
    const errorMessageStart = "Start has to be before end";
    const errorMessageEnd = "End has to be after start";

    cy.visit("");
    cy.contains(
      "Microsoft Corporation (MSFT) Stock Prices, Dividends and Splits"
    );
    cy.contains(errorMessageStart).should("not.exist");
    cy.contains(errorMessageEnd).should("not.exist");
    cy.get(selector).clear();
    cy.get(selector).type("2020-02-01");
    cy.contains(errorMessageStart).should("be.visible");
    cy.contains(errorMessageEnd).should("be.visible");
    cy.get(selector).type("2014-02-01");
    cy.contains(errorMessageStart).should("not.exist");
    cy.contains(errorMessageEnd).should("not.exist");
  });
});

describe("Handles errors", () => {
  it("Handles quandl api errors", () => {
    cy.intercept("https://www.quandl.com/api/v3/datasets/EOD/MSFT.json*", {
      statusCode: 403,
      headers: { "content-type": "application/json; charset=utf-8" },
      body: JSON.stringify({
        quandl_error: { message: "Something wasn't right", code: "E1234" },
      }),
    });

    cy.visit("");
    cy.contains("Error while loading data");
    cy.contains("Something wasn't right");
  });

  it("Handles network going offline", () => {
    cy.intercept("https://www.quandl.com/api/v3/datasets/EOD/MSFT.json*", {
      statusCode: 200,
      headers: { "content-type": "application/json; charset=utf-8" },
      body: JSON.stringify(quandlSuccessStub),
    });
    cy.intercept("https://www.quandl.com/api/v3/datasets/EOD/MSFTa.json*", {
      forceNetworkError: true,
    });

    cy.visit("");

    // Verify initial request succeeds
    cy.contains(
      "Microsoft Corporation (MSFT) Stock Prices, Dividends and Splits"
    );

    // Cause a second request and verify it failed with a network error
    cy.get("#symbol").type("a");
    cy.contains("Failed to fetch");
  });
});

describe("URL params", function () {
  it("should load chart according to url params", function () {
    cy.intercept("https://www.quandl.com/api/v3/datasets/EOD/DIS.json*", {
      statusCode: 200,
      headers: { "content-type": "application/json; charset=utf-8" },
      body: JSON.stringify(quandlSuccessStub),
    });

    cy.visit("/#?symbol=DIS&start=2014-03-03&end=2016-02-02");
    cy.get("#symbol").should("be.visible").should("have.value", "DIS");
    cy.get("#startDate")
      .should("be.visible")
      .should("have.value", "2014-03-03");
    cy.get("#endDate").should("be.visible").should("have.value", "2016-02-02");
    cy.contains(
      // Mock data still returns MSFT
      "Microsoft Corporation (MSFT) Stock Prices, Dividends and Splits"
    );
  });
});
