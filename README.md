# Adjusted Close Prices

The app shows the adjusted close price of a stock over time between a start and an end date as well as the following KPIs:
- Maximum drawdown rate
- Simple return rate

## Tech

The app is based on create-react-app, see below for details.

- TypeScript for static type checking
- jest for unit tests: [getMaxDrawdown.test.ts](src/getMaxDrawdown.test.ts), [getSimpleReturn.test.ts](src/getSimpleReturn.test.ts)
- cypress for E2E tests: [smoke test](cypress/integration/index.ts)
- Gitlab-CI for CI/CD: [.gitlab-ci.yml](.gitlab-ci.yml)
- Gitlab Pages for simple hosting

**NOTE**: To run the app locally, copy `.env` to `.env.local` and add a Quandl API Key.

# Setup
Run `yarn` to install dependencies, then `yarn start` to run the project in development mode locally.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
