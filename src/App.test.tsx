import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders stock symbol input", () => {
  render(<App />);
  const stockInput = screen.getByText(/Stock Symbol/i);
  expect(stockInput).toBeInTheDocument();
});

test("renders start date input", () => {
  render(<App />);
  const stockInput = screen.getByText(/Start/i);
  expect(stockInput).toBeInTheDocument();
});

test("renders end date input", () => {
  render(<App />);
  const stockInput = screen.getByText(/End/i);
  expect(stockInput).toBeInTheDocument();
});
