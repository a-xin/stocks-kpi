import React from "react";
import "./App.css";
import { Card, CardContent, Typography } from "@material-ui/core";

export default function ErrorCard({
  children,
  title = "Error while loading data",
}: {
  children: React.ReactElement;
  title?: React.ReactNode;
}) {
  return (
    <Card className="App-card" variant="outlined">
      <CardContent>
        <Typography variant="h5" component="h2">
          {title}
        </Typography>
        <Typography color="error">{children}</Typography>
      </CardContent>
    </Card>
  );
}
