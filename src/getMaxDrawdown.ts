const ADJ_CLOSE_COLUMN = 1;

export default function getMaxDrawdown(
  stock: {
    data: Array<[string, number]>;
    column_names: string[];
  } | null
): number | null {
  if (stock == null) {
    return null;
  }
  const { data, column_names } = stock;

  if (column_names[ADJ_CLOSE_COLUMN] !== "Adj_Close") {
    throw new Error(
      `Expected "Adj_Close" column at position ${ADJ_CLOSE_COLUMN} but got: ${JSON.stringify(
        column_names
      )} `
    );
  }

  if (data.length === 0) {
    return null;
  }

  let peak = null;
  let maximumDrawDown = 0;
  for (let i = 0; i < data.length; i++) {
    const value = data[i][ADJ_CLOSE_COLUMN];
    if (peak === null || value > peak) {
      peak = value;
    }
    const drawDown = (value - peak) / peak;
    if (drawDown < maximumDrawDown) {
      maximumDrawDown = drawDown;
    }
  }

  return maximumDrawDown;
}
