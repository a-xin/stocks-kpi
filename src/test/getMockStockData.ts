import type { QuandlEOD } from "../useFetchQuandl";

export default function getMockStockData(
  data: Array<[string, number]>
): QuandlEOD {
  return {
    name: "SomeName",
    start_date: "2014-01-02",
    end_date: "2014-01-10",
    refreshed_at: "2014-01-10",
    column_names: ["Date", "Adj_Close"],
    data,
  };
}
