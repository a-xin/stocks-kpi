import React, { useMemo } from "react";
import "./ResultCard.css";
import {
  Card,
  CardContent,
  List,
  ListItem,
  ListItemText,
  Typography,
} from "@material-ui/core";
import Chart from "./Chart";
import getMaxDrawdown from "./getMaxDrawdown";
import getSimpleRateOfReturn from "./getSimpleReturn";
import { parseISO } from "date-fns";
import { QuandlEOD } from "./useFetchQuandl";

function formatPercentage(x: number | null) {
  if (x == null) {
    return "n/a";
  }
  return `${(x * 100).toFixed(2)} %`;
}

function formatDate(date: string): string {
  return parseISO(date).toDateString();
}

export default function ResultCard({
  children: stock,
}: {
  children: QuandlEOD;
}): React.ReactElement {
  const maxDrawdown = useMemo(() => getMaxDrawdown(stock), [stock]);
  const simpleReturn = useMemo(() => getSimpleRateOfReturn(stock), [stock]);

  return (
    <Card className="Result-card" variant="outlined">
      <CardContent>
        <Typography color="textSecondary" gutterBottom>
          Refreshed at {new Date(stock.refreshed_at).toLocaleString()}
        </Typography>
        <Typography variant="h5" component="h2">
          {stock.name}
        </Typography>
        <List dense={true}>
          <ListItem>
            <ListItemText
              primary="Maximum Drawdown"
              secondary={formatPercentage(maxDrawdown)}
            />
          </ListItem>
          <ListItem>
            <ListItemText
              primary="Simple Return Rate"
              secondary={formatPercentage(simpleReturn)}
            />
          </ListItem>
          <ListItem>
            <ListItemText
              primary="Period"
              secondary={`${formatDate(stock.start_date)} - ${formatDate(
                stock.end_date
              )}`}
            />
          </ListItem>
        </List>
        <Chart stock={stock} />
      </CardContent>
    </Card>
  );
}
