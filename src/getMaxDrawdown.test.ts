import getMaxDrawdown from "./getMaxDrawdown";
import getMockStockData from "./test/getMockStockData";

test("calculates max drawdown for increase only", () => {
  const stock = getMockStockData([
    ["2014-01-02", 1],
    ["2014-01-03", 2],
    ["2014-01-06", 3],
  ]);
  expect(getMaxDrawdown(stock)).toBe(0);
});

test("calculates max drawdown for 50%", () => {
  const stock = getMockStockData([
    ["2014-01-02", 1],
    ["2014-01-03", 2],
    ["2014-01-06", 1],
    ["2014-01-06", 2],
  ]);
  expect(getMaxDrawdown(stock)).toBe(-0.5);
});

test("calculates max drawdown for 100%", () => {
  const stock = getMockStockData([
    ["2014-01-02", 1],
    ["2014-01-03", 2],
    ["2014-01-06", 0],
  ]);
  expect(getMaxDrawdown(stock)).toBe(-1);
});

test("calculates max drawdown for empty data", () => {
  const stock = getMockStockData([]);
  expect(getMaxDrawdown(stock)).toBe(null);
});

test("calculates max drawdown for single item", () => {
  const stock = getMockStockData([["2014-01-02", 1]]);
  expect(getMaxDrawdown(stock)).toBe(0);
});

test("calculates max drawdown for real example", () => {
  const stock = getMockStockData([
    ["2014-01-02", 32.100342378154586],
    ["2014-01-03", 31.884382055373678],
    ["2014-01-06", 31.210585848297242],
  ]);
  expect(getMaxDrawdown(stock)).toBe(-0.027717976318621905);
});
