import getSimpleReturn from "./getSimpleReturn";
import getMockStockData from "./test/getMockStockData";

test("calculates simple return zero", () => {
  const stock = getMockStockData([
    ["2014-01-02", 1],
    ["2014-01-03", 1],
  ]);

  expect(getSimpleReturn(stock)).toBe(0);
});

test("calculates simple return double", () => {
  const stock = getMockStockData([
    ["2014-01-02", 1],
    ["2014-01-03", 3],
  ]);

  expect(getSimpleReturn(stock)).toBe(2);
});

test("calculates simple return half", () => {
  const stock = getMockStockData([
    ["2014-01-02", 1],
    ["2014-01-03", 1.5],
  ]);

  expect(getSimpleReturn(stock)).toBe(0.5);
});

test("calculates simple return minus half", () => {
  const stock = getMockStockData([
    ["2014-01-02", 2],
    ["2014-01-03", 1],
  ]);

  expect(getSimpleReturn(stock)).toBe(-0.5);
});

test("calculates simple return for real example", () => {
  const stock = getMockStockData([
    ["2014-01-02", 32.100342378154586],
    ["2014-01-03", 31.884382055373678],
    ["2014-01-06", 31.210585848297242],
    ["2014-01-07", 31.452461409811857],
  ]);

  expect(getSimpleReturn(stock)).toBe(-0.020182992465016047);
});
