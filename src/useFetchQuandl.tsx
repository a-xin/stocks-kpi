import React, { useEffect, useState } from "react";
import "./App.css";
import { useDebounce } from "use-debounce/lib";
import useFetch from "react-fetch-hook";

export interface QuandlEOD {
  name: string;
  data: Array<[string, number]>;
  column_names: string[];
  refreshed_at: string;
  start_date: string;
  end_date: string;
}

const QUANDL_KEY = process.env.REACT_APP_QUANDL_KEY;
if ((QUANDL_KEY?.length ?? 0) < 20) {
  throw new Error("Missing QUANDL_KEY");
}

function getQuandlUrl({
  startDate,
  endDate,
  symbol,
}: {
  startDate: string;
  endDate: string;
  symbol: string;
}): string | null {
  if (!symbol || !startDate || !endDate) {
    return null;
  }

  const baseUrl = `https://www.quandl.com/api/v3/datasets/EOD/${encodeURIComponent(
    symbol
  )}.json`;

  const url = new URL(baseUrl);
  Object.entries({
    start_date: startDate,
    end_date: endDate,
    api_key: QUANDL_KEY,
    column_index: 11,
    order: "asc",
  }).forEach(([key, value]) => url.searchParams.set(key, String(value)));

  return url.toString();
}

async function formatter(response: Response) {
  if (!response.ok) {
    const data = await response.json();
    throw new Error(data.quandl_error.message);
  }
  const result = await response.json();
  if (result.dataset?.column_names?.length !== 2) {
    throw new Error("API returned invalid columns");
  }
  if (result.dataset.column_names[1] !== "Adj_Close") {
    throw new Error(
      "API returned invalid column[1]: " + result.dataset.column_names[1]
    );
  }
  return result;
}

export default function useFetchQuandl({
  startDate,
  endDate,
  symbol,
  debounce = 500,
}: {
  startDate: string;
  endDate: string;
  symbol: string;
  debounce?: number;
}): {
  isLoading: boolean;
  stock?: QuandlEOD;
  fetchError: React.ReactElement | null;
} {
  // Keep track of initial fetch to send it without debounce
  const [isInitial, setIsInitial] = useState(true);

  const url = getQuandlUrl({ startDate, endDate, symbol });
  const [fetchUrl] = useDebounce(url, debounce, { leading: isInitial });

  const { isLoading, error, data } = useFetch<{ dataset?: QuandlEOD }>(
    fetchUrl ?? "",
    {
      depends: [fetchUrl],
      formatter,
    }
  );

  useEffect(() => {
    if (fetchUrl == null) {
      return;
    }
    // Set initial to false to debounce following requests
    setIsInitial(false);
  }, [fetchUrl]);

  return {
    stock: data?.dataset,
    isLoading,
    fetchError: error == null ? null : <span>{error.message}</span>,
  };
}
