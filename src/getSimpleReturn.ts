export default function getSimpleReturn(
  stock: {
    data: Array<[string, number]>;
    column_names: string[];
  } | null
): number | null {
  if (stock == null) {
    return null;
  }
  const { data, column_names } = stock;
  const column = column_names.findIndex((name) => name === "Adj_Close");
  if (column !== 1) {
    throw new Error(
      "Data is missing Adj_Close column at position 1 " + column_names
    );
  }
  if (data.length === 0) {
    return 0;
  }
  return (data[data.length - 1][column] - data[0][column]) / data[0][column];
}
