import { useEffect, useState } from "react";
import "./App.css";
import { Grid, TextField } from "@material-ui/core";
import { isBefore, parseISO } from "date-fns";

function getSymbolErrors(
  symbol: string
): {
  hasSymbolError?: boolean;
  symbolHelperText?: string;
} {
  return symbol === ""
    ? { hasSymbolError: true, symbolHelperText: "Enter a stock symbol" }
    : {};
}

function getDateErrors({
  startDate,
  endDate,
}: {
  startDate: string;
  endDate: string;
}): {
  hasStartDateError?: boolean;
  hasEndDateError?: boolean;
  startDateHelperText?: string;
  endDateHelperText?: string;
} {
  const isStartDateValid = startDate !== "";
  const isEndDateValid = endDate !== "";
  if (!isStartDateValid || !isEndDateValid) {
    return {
      hasStartDateError: !isStartDateValid,
      startDateHelperText: !isStartDateValid ? "Enter valid date" : undefined,
      hasEndDateError: !isEndDateValid,
      endDateHelperText: !isEndDateValid ? "Enter valid date" : undefined,
    };
  }

  if (isBefore(parseISO(endDate), parseISO(startDate))) {
    return {
      hasStartDateError: true,
      startDateHelperText: "Start has to be before end",
      hasEndDateError: true,
      endDateHelperText: "End has to be after start",
    };
  }

  return {};
}

function Form({
  onChange,
  initialSymbol = "",
  initialStartDate = "",
  initialEndDate = "",
}: {
  initialSymbol?: string;
  initialStartDate?: string;
  initialEndDate?: string;
  onChange({
    symbol,
    startDate,
    endDate,
  }: {
    symbol: string;
    startDate: string;
    endDate: string;
  }): void;
}) {
  const [symbol, setSymbol] = useState<string>(initialSymbol);
  const [startDate, setStartDate] = useState<string>(initialStartDate);
  const [endDate, setEndDate] = useState<string>(initialEndDate);

  const [hasStartDateFocus, setStartDateFocus] = useState<boolean>(false);
  const [hasEndDateFocus, setEndDateFocus] = useState<boolean>(false);
  const [hasSymbolFocus, setSymbolFocus] = useState<boolean>(false);

  const {
    hasStartDateError,
    startDateHelperText,
    hasEndDateError,
    endDateHelperText,
  } = getDateErrors({
    startDate,
    endDate,
  });
  const { hasSymbolError, symbolHelperText } = getSymbolErrors(symbol);

  useEffect(() => {
    if (hasSymbolError || hasStartDateError || hasEndDateError) {
      onChange({ symbol: "", startDate: "", endDate: "" });
    } else {
      onChange({ symbol, startDate, endDate });
    }
  }, [
    endDate,
    hasEndDateError,
    hasStartDateError,
    hasSymbolError,
    onChange,
    startDate,
    symbol,
  ]);

  return (
    <form noValidate autoComplete="off">
      <Grid container justify="space-around" spacing={3}>
        <TextField
          id="symbol"
          autoFocus={true}
          label="Stock Symbol"
          onChange={(event) => setSymbol(event.target.value)}
          onFocus={() => setSymbolFocus(true)}
          onBlur={() => setSymbolFocus(false)}
          value={symbol}
          margin="normal"
          error={!hasSymbolFocus && hasSymbolError}
          helperText={symbolHelperText}
        />
        <TextField
          id="startDate"
          label="Start"
          type="date"
          InputLabelProps={{
            shrink: true,
          }}
          value={startDate}
          onChange={(event) => setStartDate(event.target.value)}
          onFocus={() => setStartDateFocus(true)}
          onBlur={() => setStartDateFocus(false)}
          margin="normal"
          error={hasStartDateError && !hasStartDateFocus}
          helperText={startDateHelperText}
        />
        <TextField
          id="endDate"
          label="End"
          type="date"
          InputLabelProps={{
            shrink: true,
          }}
          value={endDate}
          onChange={(event) => setEndDate(event.target.value)}
          onFocus={() => setEndDateFocus(true)}
          onBlur={() => setEndDateFocus(false)}
          margin="normal"
          error={hasEndDateError && !hasEndDateFocus}
          helperText={endDateHelperText}
        />
      </Grid>
    </form>
  );
}

export default Form;
