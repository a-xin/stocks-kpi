import { lazy, Suspense, useCallback } from "react";
import "./App.css";
import { LinearProgress } from "@material-ui/core";
import Form from "./Form";
import useFetchQuandl from "./useFetchQuandl";
import ErrorCard from "./ErrorCard";
import useHashParam from "use-hash-param";

// ResultCard depends on chartjs which is >80kb
// Code-split but prefetch to load it in parallel with data
const ResultCard = lazy(() =>
  import(/* webpackPrefetch: true */ "./ResultCard")
);

function App() {
  const [symbol, setSymbol] = useHashParam("symbol", "MSFT");
  const [startDate, setStartDate] = useHashParam("start", "2014-01-01");
  const [endDate, setEndDate] = useHashParam("end", "2017-01-01");

  const setFormData = useCallback(
    ({ symbol, startDate, endDate }) => {
      setSymbol(symbol);
      setStartDate(startDate);
      setEndDate(endDate);
    },
    [setStartDate, setSymbol, setEndDate]
  );

  const { stock, isLoading, fetchError } = useFetchQuandl({
    startDate,
    endDate,
    symbol,
  });

  return (
    <div className="App">
      <header className="App-header">Stocks</header>

      <Form
        initialSymbol={symbol}
        initialStartDate={startDate}
        initialEndDate={endDate}
        onChange={setFormData}
      />

      <div className="App-loading">{isLoading && <LinearProgress />}</div>

      {fetchError != null && <ErrorCard>{fetchError}</ErrorCard>}

      {stock != null && (
        <Suspense fallback={<div>Loading...</div>}>
          <ResultCard>{stock}</ResultCard>
        </Suspense>
      )}
    </div>
  );
}

export default App;
