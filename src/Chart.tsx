import React, { useMemo } from "react";
import "./App.css";
import { Line } from "react-chartjs-2";
import "chartjs-adapter-date-fns";

function formatCurrency(value: number): string {
  return value.toLocaleString("en-US", { style: "currency", currency: "USD" });
}

function formatLabel(item: {
  parsed: { x: number; y: number };
  dataset: { label: string };
}) {
  return `${item.dataset.label}: ${formatCurrency(item.parsed.y)}`;
}

const chartOptions = {
  scales: {
    xAxis: {
      type: "time",
      distribution: "linear",
      ticks: { source: "auto" },
    },
    yAxis: {
      ticks: {
        beginAtZero: true,
        callback: formatCurrency,
      },
    },
  },
  plugins: {
    tooltip: {
      callbacks: {
        label: formatLabel,
      },
    },
  },
};

function Chart({ stock }: { stock: { data: Array<[string, number]> } }) {
  const chartData = useMemo(() => {
    const labels = stock?.data.map(([key, _value]) => key);
    const data = stock?.data.map(([_key, value]) => value);
    return {
      labels,
      datasets: [
        {
          label: "Adjusted Close Price",
          data,
          fill: false,
          backgroundColor: "rgb(255, 99, 132)",
          borderColor: "rgba(255, 99, 132, 0.2)",
          type: "line",
        },
      ],
    };
  }, [stock]);

  return <Line type="line" data={chartData} options={chartOptions} />;
}

export default React.memo(Chart);
